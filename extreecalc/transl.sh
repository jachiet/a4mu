# preprocessing in O(n) but requires to call solver so accidently O(2^n)
for i in e9 e10 e11 e12 ; do
    ./trad.sh $i > $i.mu
    (echo '~' ; cat $i.mu) > n$i.mu 
done;

for i in e9 e10 e11 e12 ; do
    echo "" ; echo "------------------------------------------------" ;
    echo "Translation to automaton $i…"
    time -p ../trad/solver $i.mu compress > $i.auto
    echo "" ; echo "------------------------------------------------" ;
    echo "Translation to automaton n$i…"
    time -p ../trad/solver n$i.mu compress > n$i.auto
done  ;

echo "" ; echo "------------------------------------------------" ;
echo "Intersecting everything!";

echo "e9 ⊑ e10 U e11 U e12 => non sat of e9 ∩ ¬ e10 ∩ ¬ e11  ∩ ¬ e12 "
time -p (((((cat e9.auto ne10.auto | ../interAuto/inter) ;(cat ne11.auto ne12.auto | ../interAuto/inter)) | ../interAuto/inter ) ) | sed 's/.context//' ) | ../example/get_example_label.sh
echo "" ; echo "" ;
echo "e9 ⊑ e10 U e11 U e12 modulo XHTML => non sat of  e9 ∩ ¬ e10 ∩ ¬ e11  ∩ ¬ e12 ∩ XHTML "
time -p ((((((cat e9.auto ne10.auto | ../interAuto/inter) ;(cat ne11.auto ne12.auto | ../interAuto/inter)) | ../interAuto/inter ) ) | sed 's/.context//' ) ;cat ../xhtml_strict.auto) | ../intersect/inter

echo "" ; echo "------------------------------------------------" ;

echo "Is it possible to nest <a> tag in XHTML ? e8 ∩ XHTML";
bash e8.sh

