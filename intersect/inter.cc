#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <set>
#include <deque>
#include <queue>


using namespace std ;

void copy( vector<int> & a, vector<int> & b)
{
  a.insert(a.end(),b.begin(),b.end());
  a.resize(distance(a.begin(),unique(a.begin(),a.end())));
  b.clear();
}


struct automaton
{
  int start1 ;
  int start2 ;
  int end ;
  int nb_states,nb_labels ;
  vector<int> * trans ;
  vector<string> labels ;
  vector<string> prec_labels ;
  map<string,int> assoc ;
  int other ;
  vector<int> rename ;
  map<pair<int,int>,set<string> > labels_of ;
  
  automaton ()
  {
    other = -1 ;
    char buf[200] ;
    int nb;
    scanf("%d",&nb_labels);
    for(int l = 0 ; l < nb_labels ; l++)
      {
        scanf("%s",buf);
        assoc[string(buf)] = labels.size() ;
        if(string(buf) == string("_other"))
          other = labels.size();
        labels.push_back(string(buf));
      }  
    if(other<0)
      {
	other = labels.size() ;
	labels.push_back("_other");
	nb_labels++;
      }
    scanf("%d %d %d %d %d\n",&nb_states,&start1,&start2,&end,&nb);
    trans = new vector<int>[nb_states*nb_states*nb_labels] ;
    while(nb--)
      {
        int e1, e2, e3;
        scanf("%s %d %d %d",buf,&e1,&e2,&e3);
        trans[(e1*nb_states+e2)*nb_labels+assoc[buf]].push_back(e3);
	labels_of[make_pair(e1*nb_states+e2,e3)].insert(buf) ;
      }
  }
  
  void to_rename(string s,int from)
  {
    int dest = other ;
    if(assoc.find(s) != assoc.end())
      dest = assoc[s] ;
    rename[from] = dest ;
  }

  void sort_tr()
  {
    for(int i = 0 ; i < nb_states*nb_states*nb_labels ; i++)
      sort(trans[i].begin(),trans[i].end());
  }

  vector<int> & get_trans(const int e1, const int e2, const int l)
  {
    return trans[(e1*nb_states+e2)*labels.size()+rename[l]];
  }

  void merge(int l)
  {
    //printf("Merging %s \n",labels[l].c_str());
    for(int e1 = 0 ; e1 < nb_states ; e1++ )
      for(int e2 = 0 ; e2 < nb_states ; e2++ )
	{
	  copy(trans[(e1*nb_states+e2)*nb_labels+other],trans[(e1*nb_states+e2)*nb_labels+l]);
	}
    labels[l] = "_other";
  }
  
} ;

vector<string> all_labels ;
automaton a, b;

void compute_labels()
{
  for(size_t i = 0 ; i < b.labels.size() ; i++ )
    {
      if(a.assoc.find(b.labels[i]) == a.assoc.end())
	b.merge(i);
    }

  for(size_t i = 0 ; i < a.labels.size() ; i++ )
    {
      if(b.assoc.find(a.labels[i]) == b.assoc.end())
	a.merge(i);
    }
  all_labels = a.labels ;
  all_labels.insert(all_labels.end(),b.labels.begin(),b.labels.end());
  sort(all_labels.begin(),all_labels.end());
  all_labels.resize(distance(all_labels.begin(),unique(all_labels.begin(),all_labels.end())));

  a.rename.resize(all_labels.size());
  b.rename.resize(all_labels.size());
  
  for(size_t i = 0 ; i < all_labels.size() ; i++ )
    {
      //printf("Label %s\n",all_labels[i].c_str());
      a.to_rename(all_labels[i],i);
      b.to_rename(all_labels[i],i);
    }

  //a.sort_tr() ;
  //b.sort_tr() ;
}

vector<pair<int,int> > todo;
vector<int> height ;
vector<int> son[3];
set<pair<int,int> > seen ;
priority_queue<pair<int,pair<int,int>>> next_couple ;

void show_path(const int i)
{
  if(son[2][i]<0)
    {
      printf("#");
      return ;
    }
  set<string> atrans = a.labels_of[make_pair(todo[son[0][i]].first*a.nb_states+todo[son[1][i]].first,todo[i].first)];
  set<string> btrans = b.labels_of[make_pair(todo[son[0][i]].second*b.nb_states+todo[son[1][i]].second,todo[i].second)];
  string label = "" ;
  for( string s : atrans )
      if( btrans.count(s) || (btrans.count("_other") && (!b.assoc.count(s)) ) )
	{
	  label = s; 
	  btrans.clear();
	}

  if(label=="")
    for( string s : btrans )
	if( !a.assoc.count(s))
	  label = s ;

  printf("%s(",label.c_str());
  show_path(son[0][i]);
  printf(",");
  show_path(son[1][i]);  
  printf(")");
}

inline void add( const pair<int,int> s, const int i, const int j,const int l)
{
  if(!seen.count(s))
    {
      

      int cur_height = -1 ;
      if(j>=0)
	cur_height += height[j] ;
      if(i>=0)
	cur_height += height[i] ;

      height.push_back(cur_height);
      
      for(size_t o = 0 ; o < todo.size() ; o++ )
	  next_couple.push( make_pair(cur_height + height[o], make_pair(todo.size(),o)));
      next_couple.push( make_pair(2*height.back(), make_pair(todo.size(),todo.size())));

      todo.push_back(s);
      seen.insert(s);
      son[0].push_back(i);
      son[1].push_back(j);
      son[2].push_back(l);
      if(s.first == a.end && s.second == b.end)
	{
	  printf("NON EMPTY!\n");
	  show_path(todo.size()-1);
	  printf("\n");
	  exit(0);
	}

    }
}

inline void do_pair(const size_t i, const size_t j)
{
  const pair<int,int> s1 = todo[i] ;
  const pair<int,int> s2 = todo[j] ;
  for(size_t l = 0 ; l < all_labels.size() ; l++)
    for(int e1 : a.get_trans(s1.first,s2.first,l))
      for(int e2 : b.get_trans(s1.second,s2.second,l))
	add(make_pair(e1,e2),i,j,l);
}

void inter()
{
  add(make_pair(a.start1,b.start1),-1,-1,-1);
  add(make_pair(a.start2,b.start2),-1,-1,-1);
  while( !next_couple.empty() )
    {
    pair<int,int> e = next_couple.top().second;
    next_couple.pop();
    do_pair(e.first,e.second) ;
    if(e.first != e.second )
      do_pair(e.second,e.first) ;
  }
  printf("EMPTY!\n");
}

int main(int argc, char ** argv)
{
  compute_labels();
  inter();
  return 0;
}
