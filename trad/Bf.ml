type t = 
  | And of t * t
  | Or of t * t
  | Litt of int * bool
  | Top 
  | Bot

type af =
  | Af_Prog of int*t
  | Af_Atom of string 

let rec neg = function
  | And(a,b) -> Or(neg a, neg b)
  | Or(a,b) -> And(neg a, neg b)
  | Litt(i,b) -> Litt(i,not b)
  | Top -> Bot
  | Bot -> Top


let xor a v = 
  if a then neg v else v


let rec replace i v = function
  | And(a,b) -> And(replace i v a,replace i v b)
  | Or(a,b) -> Or(replace i v a, replace i v b)
  | Litt(i',b) -> if i' = i then xor b v else Litt(i',b)
  | f -> f

let rec simplify = function
  | And(a,b) -> 
     (match (simplify a, simplify b) with
     | Top,f | f,Top-> f
     | Bot,_ | _,Bot -> Bot
     | a,b -> And(a,b))
  | Or(a,b) -> 
     (match (simplify a, simplify b) with
     | Top,f | f,Top-> Top
     | Bot,f | f,Bot -> f
     | a,b -> Or(a,b))
  | f -> f

let rec get_min_var = function
  | And(a,b) | Or(a,b) -> min (get_min_var a) (get_min_var b)
  | Litt(i,b) -> i
  | f -> -1

let print f = 

  let rec foo = function
  | And(a,b) -> "("^foo a^" ʌ "^foo b^")"
  | Or(a,b) -> "("^foo a^" v "^foo b^")"
  | Litt(a,b) -> (if b then "~" else "")^"V"^string_of_int a
  | Top -> "T"
  | Bot -> "B"
  in
  print_string (foo f) ; print_newline () ; flush_all ()  

let af_print = function
  | Af_Atom s ->  print_string ("Atom "^s^"\n") ; print_newline () ; 
  | Af_Prog (i,f) -> print_string "<";print_int i;print_string "> "; print f ; print_newline ()
  
