(* Analyseur lexical pour mu-calcul *)
{
  open Parser

  exception Lexing_error of string

  let kwd_tbl = ["let", LET; "in", IN; "mu", MU;"T", TRUE ; ".",POINT ]
  let id_or_kwd s = try List.assoc s kwd_tbl with _ -> IDENT s
  let line = ref 0 
  let newline () = incr line
}

let alphanum = ['a'-'z' 'A'-'Z' '_' '.' ]['a'-'z' 'A'-'Z' '0'-'9' '_']*
let intprog = '-'?['1' '2']
let space = ' ' | '\t'

rule next_token = parse
  | '\n'
      { newline () ; next_token lexbuf }
  | space+
      { next_token lexbuf }
  | alphanum as id { id_or_kwd id }
  | '='     { EQUAL }
  | '$'     { VAR }  
  | '|'     { OR }
  | '&'     { AND }
  | ','     { COMMA }
  | '~'     { NOT }
  | '('     { LEFTPAR }
  | ')'     { RIGHTPAR }
  | '<'     { LEFTPROG }
  | '>'     { RIGHTPROG }
  | intprog as s
	    { PROG(int_of_string s) }
  | eof     { EOF }
  | _ as c  { raise (Lexing_error ("illegal character: " ^ String.make 1 c)) }

{}



