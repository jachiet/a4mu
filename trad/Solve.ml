(* TODO : Memoize *)

let solve debug  stats form plean =  
  
  let seen = Array.make 3 Sol.none
  and todo = Array.make 3 [] 
  and made = Array.make 3 [] in

  let lean_size = List.length plean in
  
  
  let atom_val = Array.make lean_size None in
  let is_atom = Array.make lean_size false in

  let atom_pos =
    let foo l (e,i) = match e with
      | Bf.Af_Atom s ->  is_atom.(i) <- true; (s,i)::l
      | _ -> l
    in
    List.fold_left foo [] plean
  in

  let atom_assoc =
    let foo l (e,i) = match e with
      | Bf.Af_Atom s ->  (i,s)::l
      | _ -> l
    in
    List.fold_left foo [] plean
  in

  let prog modas = 
    let foo l (form,id) = match form with
      | Bf.Af_Prog(i,f) ->
	 if List.mem i modas
	 then (Bf.Litt (id,false))::l	
	 else l
      | _ -> l
    in
    List.fold_left foo [] plean
  in
  
  let prog_form m =  
    Bf.simplify (List.fold_left (fun ac el -> Bf.Or(ac,el)) Bf.Bot (prog m))
  in

  (* neg_prog_form L prevents all formulas <i>φ to be true for i∈L *)
  let neg_prog_form m =
    Bf.neg (prog_form m) 
  in

  let root_form = neg_prog_form  [-1;-2] in

  let bot_form = neg_prog_form [1;2] in

  let no_parent = [| Sol.none ; Sol.get (neg_prog_form [-1]) ; Sol.get (neg_prog_form [-2]) |] in

  let bf_sol_form = Bf.And(root_form, form ) in
  
  let sol_form = Sol.get (bf_sol_form) in

  let show f =
    print_string "Solutions pour la formule : " ;
    Bf.print f ;
    Sol.print (Sol.get f) 
  in

  let interface = 
    let interface moda = (* moda = 1 or moda = 2 *)
      let foo l (form,id) = match form with
	| Bf.Af_Prog(i,f) -> 
	   if i = moda 
	   then (f,Bf.Litt(id,false))::l
	   else
	     begin
	       if i = (-moda)
	       then (Bf.Litt(id,false), f)::l
	       else l
	     end
	| Bf.Af_Atom _ -> l
      in
      List.fold_left foo [] plean 
    in
    [|[];interface 1; interface 2|]
  in

  let solved_interface =     
    let sol l = 
      List.mapi (fun i (x,y) -> Sol.get x,i,Sol.get y, Sol.get (Bf.neg y)) l
    in 

    Array.map sol interface 
  in
  
   let fixed = Array.make (max (List.length solved_interface.(1))(List.length solved_interface.(2))) false in

  
  let rec combine flist =
    let l = List.map (fun (a,i,x,y) -> if fixed.(i) then x else y) flist in
    List.fold_left Sol.inter Sol.all l 
  in

  let no_child_form =
    if false 
    then Array.map combine solved_interface
    else [|Sol.none;Sol.get (neg_prog_form [1]); Sol.get (neg_prog_form [2])|] 
  in
  
  let compute c (s1,s2) flist sol = 

    let mark last_var_set set_sol =

      let a = combine flist in

      (* For each atom i we have set atom.(i) to bool Option mark_all
        adds all the transition corresponding to the labels defined by
        those atoms *)
      let rec mark_all limit cur = function
	| [] -> Auto.add_transition cur s1 s2 a c
	| (s,i)::q ->
	   if i >= limit
	   then atom_val.(i) <- None ; 
	   match atom_val.(i) with
	   | None -> (mark_all limit cur q ; mark_all limit (s::cur) q)
	   | Some(true) -> mark_all limit (s::cur) q
	   | Some(false) -> mark_all limit cur q
      in

      (* We have found a transition from a set of transitions S but
      maybe not all atoms are set, gen_all generate all variations of
      atoms compatible with the the set S *)
      let rec gen_all i s =
        match Sol.get_type s with
        | Sol.Bot -> ()
        | Sol.Top -> mark_all i [] atom_pos
        | _ ->
           List.iter
             (fun b ->
              let f = Sol.cut b s in
              atom_val.(i) <- Some b ;
              gen_all (i+1) f ;
              atom_val.(i) <- None) [false;true]
      in

      gen_all last_var_set set_sol ;
      if  not (Sol.mem fixed seen.(c))
      then
	begin
	  (* discovered a new accessible state *)
	  seen.(c) <- Sol.add fixed seen.(c) ;
	  todo.(c) <- a::todo.(c)
	end
    in

    (* Replace all occurences of the variable 'i' by 'v' in the list
    given and replace in the array fixed formula by  *)
    let rec setvar_list s b new_forms is_fixed = function
      | [] -> s,new_forms,is_fixed
      | (f,i,x,y)::l ->
	 let f = Sol.cut b f in
	 if f = Sol.all then (setvar_list (Sol.inter s x) b new_forms ((i,true)::is_fixed) l)
	 else if f = Sol.none then (setvar_list (Sol.inter s y) b new_forms ((i,false)::is_fixed) l)
	 else (setvar_list s b ((f,i,x,y)::new_forms) is_fixed l)
    in
  

    (* Compute the image-set of the set 'sol' by the fonction eval on
       the formulas to_fix *)
    
    let rec proj i (cur_sol,to_fix) sol =
      if sol <> Sol.none
      then
	if to_fix = []
	then mark i sol
	else
	  begin
	    let a,b = match Sol.get_type sol with
	      | Sol.Bot -> (Sol.none,Sol.none)
	      | Sol.Top ->  (Sol.all,Sol.all)
	      | Sol.Ign(a) -> (a,a)
	      | Sol.Dis(a,b) -> (a,b)
	    in
	    let (s1,l1,f1) = setvar_list cur_sol false [] [] to_fix in
	    let (s2,l2,f2)  = setvar_list cur_sol true [] [] to_fix in

	    if a<>b || s1 <> s2 || f1 <> f2 || l1 <> l2
	    then
	      begin
		atom_val.(i) <- Some false ;
		List.iter (fun (i,b) -> fixed.(i) <- b ) f1 ;
		proj (i+1) (s1,l1) a ;	
		atom_val.(i) <- Some true ; 
	      end
	    else
	      atom_val.(i) <- None ;
	    List.iter (fun (i,b) -> fixed.(i) <- b ) f2 ;
	    proj (i+1) (s2,l2) b
	      
	  end
    in    
    proj 0 (Sol.all,flist) sol
  in


  let show_stats () = 
    print_string "\nStats :\nNumber of states "; 
    print_int (List.length made.(1)) ; print_string " + " ; print_int (List.length made.(2)) ; 
    print_string "\nNumber of interfaces " ;
    print_int (Sol.get_size seen.(1)) ; 
    print_string " + "; 
    print_int (Sol.get_size seen.(2)) ;
    print_newline () ;
    let a,b = Sol.stat () in
    print_string "Memory " ; print_int (a) ; print_string " " ; print_int b; print_newline ()
  in											   
  
  let show_debug () = 
    show bot_form ;
    show root_form ;
    show form ;
    show bf_sol_form ;
    show (Bf.simplify (Bf.neg (prog_form [-1]))) ;
    show (Bf.simplify (Bf.neg (prog_form [-2]))) ;
    print_string " --- \n" ;
    Sol.print no_child_form.(1) ;
    print_string " --- \n" ;
    Sol.print no_child_form.(2) ;
    print_string " --- \n" ;
    for i = 1 to 2 do 
      print_string  "Interface " ; 
      print_int i ; 
      print_newline () ;
      let foo (x,y) = 
	Bf.print x; 
	print_string "  <= on child / on parent "; 
	print_int i ; 
	print_string "=> " ; 
	Bf.print y ; 
	print_newline() 
      in
      List.iter foo  interface.(i)
    done
  in
  
  let compute_side i (s1,s2) =
    compute i (s1,s2) solved_interface.(i) (Sol.inter (Sol.inter s1 s2) no_parent.(3-i))
  in

  let mark_final (s1,s2) =
    let sols = Sol.inter (Sol.inter s1 s2) sol_form in
    let rec foo i c s =
      if s <> Sol.none
      then
	begin
	  if i = lean_size
	  then (Auto.add_transition c s1 s2 Sol.notsol 2 )
	  else
	    begin
	      foo (i+1) c (Sol.cut false s) ;
	      foo (i+1)(if is_atom.(i) then (List.assoc i atom_assoc::c) else c) (Sol.cut true s)
	    end
	end
    in
    foo 0 [] sols
  in
  
  let do_side i = match todo.(i) with
    | [] -> false
    | a::q -> 
       todo.(i) <- q ;
       let f x = if i = 1 then (a,x) else (x,a) in
       List.iter (fun x -> compute_side 1 (f x)) made.(3-i) ;
       List.iter (fun x -> compute_side 2 (f x)) made.(3-i) ;
       List.iter (fun x -> mark_final (f x)) made.(3-i) ;
       made.(i) <- a::made.(i) ;
       true
  in

  let rec fix_point () = 
    if do_side 1 || do_side 2 then fix_point ()
  in

  if debug then show_debug () ;
  made.(1) <- [no_child_form.(1)] ;
  todo.(2) <- [no_child_form.(2)] ;
  fix_point () ;
  if stats && false then show_stats () 
  
