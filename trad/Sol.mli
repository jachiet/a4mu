type i
type t = Dis of i * i | Top | Bot | Ign of i
val get_type : i -> t
val add_type : t -> int
val add_type_ign : i -> i
val add_type_dis : i * i -> i
val get : Bf.t -> i
val inter : i -> i -> i
val cut : bool -> i -> i
val print : i -> unit
val mem : bool array -> i -> bool
val add : bool array -> i -> i
val get_size : i -> int
val none : i
val all	: i     
val notsol : i
val prints : i -> unit
val resize : int -> unit
val stat : unit -> int*int
