type formula = 
  | Atom of string
  | Neg of formula
  | And of formula * formula
  | Or  of formula * formula
  | Prog of int * formula
  | Mu  of (string * formula) list * string
  | Var of string
  | True 
             
exception Freevar of string 
	     
let replace l f =
  let rec foo = function
  | Var s -> Mu(l,s)
  | Neg f -> Neg (foo f)
  | Prog(i,f) -> Prog(i,foo f)
  | And(a,b) -> And(foo a,foo b)
  | Or(a,b)-> Or(foo a, foo b)
  | f -> f
  in
foo f

let print f =
  let rec foo = function
    | Atom s -> "_"^s
    | Neg f -> "~"^foo f
    | And (a,b) -> "("^foo a^" ʌ "^foo b^")"
    | Or (a,b) -> "("^foo a^" v "^foo b^")"
    | Prog (a,b) -> "<"^string_of_int a^">"^foo b
    | Mu(l,s) -> "let "^(List.fold_left (fun a (s,f) -> s^"="^(foo f)^","^a) "" l)^" in "^s
    | Var v -> v
    | True -> "T"
  in
  print_string (foo f)


let lean f =

  let rec add l x = match l with
    | [] -> [x]
    | t::q -> t::(if x=t then q else add q x)
  in

  let rec foo acc = function
  | Atom s -> add acc (Atom s)
  | Neg f -> foo acc f
  | Or(a,b) | And(a,b) ->  foo (foo acc a) b
  | Prog(i,f) -> 
     if List.mem (Prog(i,f)) acc 
     then acc
     else foo (add acc (Prog(i,f))) f
  | Mu(l,f) -> 
     foo acc (replace l (List.assoc f l)) 
  | True -> acc
  | Var _ -> failwith "Error, unfolded var"
  in

  let rec num = function
    | [] -> 0,[]
    | a::q -> let i,l = num q in (i+1),(a,i)::l
  in


  
  let size,nl =  num (List.sort (fun a b -> if a < b then -1 else 1 )(foo [] f)) in
  
  let rec mu_to_bf = function 
    | And(a,b) -> Bf.And(mu_to_bf a, mu_to_bf b)
    | Or(a,b)  -> Bf.Or(mu_to_bf a, mu_to_bf b)
    | Neg(a) -> Bf.neg (mu_to_bf a)
    | Mu(l,f) -> mu_to_bf (replace l (List.assoc f l))
    | Var _ -> failwith "Error, unfolded var"
    | True -> Bf.Top
    | f ->  Bf.Litt (List.assoc f nl,false)
  in

  let ren = function
    | Prog(i,f),n -> Bf.Af_Prog(i,mu_to_bf f),n
    | Atom s,n -> Bf.Af_Atom s,n
    | _ -> failwith "Abnormal lean"
  in
  
  size,List.map ren nl, mu_to_bf f


let plunge f =
  let x a = Prog(a, Var("X")) in 
  Mu([("X",Or(x 1,Or(x 2,f)))],"X")

let compress f =
  let rec foo l = function
    | And(a,b) | Or(a,b) -> foo (foo l a) b
    | Prog(_,a)| Neg(a) -> foo l a
    | Mu(lf,fo) -> List.fold_left (fun ac (_,el) -> foo ac el) l lf
    | Var _ -> l
    | True -> l
    | Atom s -> if s.[0] ='.' || List.mem s l then l else s::l
  in
  
  let atoms = foo [] f in

  (* let _ = List.iter (fun s -> print_string "++Atom " ; print_string s ; print_newline () ) atoms in *)
  
  let rec p2 i =
    if i = 1 then 1 else 2*(p2 (i/2))
  in
  
  let decomp i = 
    let rec foo b i =
      let cur = if b<=i then Atom(string_of_int b) else Neg(Atom(string_of_int b)) in
      let new_i = if b<=i then i-b else i in 
      if b <= 1
      then cur
      else And(cur,foo (b/2) (new_i))
    in
    foo (p2 (List.length atoms)) i
  in
  
  let assoc = List.mapi (fun i e -> e, decomp i) atoms in
  
  let rec mf = function
    | And(a,b) -> And(mf a, mf b)
    | Or(a,b) -> Or(mf a, mf b)
    | Prog(a,b) -> Prog(a,mf b)
    | True -> True
    | Neg(a) -> Neg(mf a)
    | Mu(l,f) -> Mu(List.map (fun (a,b) -> (a,mf b)) l,f)
    | Var s -> Var s
    | Atom s -> if s.[0]='.' then Atom s else List.assoc s assoc
  in

  let trad  n = 
    let rec foo i = function
      | [] -> if i < List.length atoms then List.nth atoms i else "_other"
      | a::q ->
	 if a.[0]='.'
	 then (foo i q)^a
	 else foo (i+(int_of_string a)) q
    in
    (foo 0 n)
  in
      
  mf f,trad
      
let testfreevar f = 
  let rec foo v = function
    | Atom s -> Atom s
    | Neg f -> Neg (foo v f)
    | And(a,b) -> And(foo v a, foo v b)
    | Or(a,b) -> Or(foo v a, foo v b)
    | Prog(a,b) -> Prog(a,foo v b)
    | True -> True
    | Mu(l,fo) -> 
       let nf= (fst (List.split l))@v in 
       let nl = List.map (fun (x,y) -> (x,foo nf y)) l in 
       Mu(nl,fo)
    | Var s -> if List.mem s v then Var(s) else raise (Freevar s)
  in
foo [] f

