open Lexing
open Parser
open Solve

let _ =   
  if (Array.length Sys.argv) <= 1
  then
    failwith "Not enough args!" ;
  let compress = ref false
  and debug = ref false
  and lean = ref false
  and plunge = ref false
  and stats = ref false 
  and det_auto = ref false in
  for i = 0 to Array.length Sys.argv - 1 do
    if Sys.argv.(i) = "compress" then compress := true ;
    if Sys.argv.(i) = "plunge" then plunge := true ;
    if Sys.argv.(i) = "determinize" then det_auto := true ;
    if Sys.argv.(i) = "stats" then stats := true ;
    if Sys.argv.(i) = "lean" then lean := true ;
    if Sys.argv.(i) = "debug" then debug := true ;
  done ;
  try
    if !debug then  print_string ("Opening file "^Sys.argv.(1)^"!\n") ;
    let c = open_in Sys.argv.(1) in
    let lb = Lexing.from_channel c in
    let f =  (Mu.testfreevar (Parser.formfile Lexer.next_token lb)) in
    let f = if !plunge then Mu.plunge f else f in 
    let f,trad = if !compress then Mu.compress f else f,(fun l -> if l = [] then "_other" else (List.fold_left (^) "" l)) in 
    close_in c;
    if !lean then  print_string "Solving formula : " ; 
    if !lean then Mu.print f ;
    let tlean,plean,form = Mu.lean f in
    Sol.resize (tlean*tlean*200+7) ;
    if !stats then
      begin
	print_string "Lean Size "; 
	print_int (List.length plean) ;
	print_newline();
      end ;      
    if !lean then
      begin
	print_string "\nLean :\n"; 
	List.iter (fun (f,i) -> print_string "  V" ; print_int i ; print_string ": " ;  Bf.af_print f) plean ;
	print_newline();  print_newline();
      end ;
    solve (!debug)  (!stats) form plean  ;
    
    if !det_auto then Auto.get_automaton trad else Auto.show_automaton trad ;
  with
  | Lexer.Lexing_error s ->
     print_string ("lexical error: "^s);
     exit 1
  | Mu.Freevar s ->
     print_string ("free variable: "^s);
     exit 1
  | Parser.Error ->
     print_string "parsing error";
     exit 1
     (*
  | e ->
     eprintf "Anomaly: %s\n@." (Printexc.to_string e);
 *)
;;
