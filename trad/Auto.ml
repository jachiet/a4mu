let uniq_id () =
  let hsh =  Hashtbl.create 10 in
  let ct = ref 0 in
  let l = ref [] in
  (fun s -> 
  try
    Hashtbl.find hsh s
  with
  | Not_found ->
     l := s::(!l) ; 
     Hashtbl.add hsh s (!ct) ; incr ct ; (!ct)-1),(fun () -> !l)
  
let (get_label : (string list -> int)),list_labels = uniq_id ()
let get_type,list_types = uniq_id ()

let get_ith l i = List.nth (List.rev l) i
	   
let trans = [|[];[];[]|]

let final = ref (-1)
	      
let set_final (s:Sol.i) = final:=(get_type (2,s)) 
	      
let add_transition label (s1:Sol.i) (s2:Sol.i) (s3:Sol.i) (c:int) =
  (* List.iter print_string label ; print_string ("|") ; print_int (get_label label) ; print_string ("|") ; print_int (get_type (1,s1)) ; print_string " " ; print_int (get_type (2,s2)) ; print_string " " ; print_int (get_type (c,s3)) ; print_newline ()  ; *)
  let e1 = get_type(1,s1) in
  let e2 = get_type(2,s2) in
  let e3 = get_type(c,s3) in
  trans.(c) <- (get_label label,e1,e2,e3)::trans.(c)
;;

  
(* 

Printing the output automaton, we remove non-coaccessible states, the
argument "trad" contains the translation from internal representation
of labels to their input (trad = identity when compression isn't used

*)
let show_automaton trad =
  set_final Sol.notsol ;
  let nb_types = (List.length (list_types ())) in
  let seen = Array.make nb_types false in
  let tr = Array.make nb_types [] in
  for i = 0 to 2 do
    List.iter (fun (l,e1,e2,e3) -> tr.(e3) <- e1::e2::tr.(e3)) trans.(i) ;
  done ;
  
  let rec foo f =
    if not seen.(f)
    then
      begin
	seen.(f) <- true ;
	List.iter foo tr.(f) ;
      end ;
    ()
  in
  foo (!final) ;
  let access = ref 0 in
  for i = 0 to nb_types -1 do
    if seen.(i) then incr access ;
  done;
  let labels = Array.of_list ((List.rev(List.map trad (list_labels())))) in
  let use_label = Array.mapi (fun i s -> (i==0) || (labels.(i-1)<>s)) labels in
  let nb_labels = Array.fold_left (fun a e-> a+(if e then 1 else 0)) 0 use_label in
  print_int (nb_labels) ;
  Array.iteri (fun i s -> if use_label.(i) then (print_string " " ; print_string s)) labels  ;
  let ftrans = (List.filter (fun (l,e1,e2,e3) -> seen.(e3) && use_label.(l))) in
  let t1 =  (ftrans trans.(1))
  and t2 = (ftrans trans.(2)) in
  print_string "\n";
  print_int nb_types ;
  print_string " 0 1 " ; (* start states *)
  print_int (!final) ;
  print_string " " ;
  print_int (List.length t1 + List.length t2) ;
  print_string "\n" ;
  let aff =
    List.iter (fun (l,e1,e2,e3) ->
               print_string labels.(l) ; 
	       (* print_int l ; *)
	       print_string " " ;
	       print_int e1 ;
	       print_string " " ;
	       print_int e2 ;
	       print_string " " ;
	       print_int e3 ;
	       print_newline ()) in
  aff t1 ; aff t2

    
    
(*

Printing a deterministic automaton
BETA_MODE not much tested !!!

*)
let get_automaton trad =
  
  let rec uniq = function
    | a::b::q -> if a=b then uniq (b::q) else a::uniq (b::q)
    | a -> a
  in

  let rec uniq_add a = function
    | [] -> [a]
    | h::t ->
       if h < a
       then h::(uniq_add a t)
       else
	 if h > a
	 then a::h::t
	 else h::t
  in
  
  let nb_labels = List.length (list_labels ()) in
  let nb_types = List.length (list_types ()) in


  let trans_semi = Array.init nb_labels (fun i -> Array.make_matrix nb_types nb_types []) in

  let split t =
    List.iter (fun (l,s1,s2,t) -> trans_semi.(l).(s1).(s2) <- uniq_add t trans_semi.(l).(s1).(s2)) t ;
  in							    
  split trans.(1) ;
  split trans.(2) ;

  set_final Sol.notsol ;
  
  let get_state_hsh,list_states = uniq_id () in 

  let get_ith_state i =
    let a =List.nth (List.rev (list_states ())) i in
    if get_state_hsh a <> i then failwith "!!" ; a 
  in
  
  let get_state e1 e2 l =
    let se1 = get_ith_state e1 in
    let se2 = get_ith_state e2 in
    
    let e = List.fold_left (fun ac x -> List.fold_left (fun ac2 y -> uniq (List.merge (fun a b -> if a < b then -1 else 1) trans_semi.(l).(x).(y) ac2)) ac se2) [] se1 in
    let s = get_state_hsh e in 
   s
  in

  let tr = ref [] in
  let _ = get_state_hsh [0;1] in
  
  let dostate a b l =
    let g = get_state a b l in
    tr := (trad (List.nth (List.rev (list_labels ())) l),a,b,g)::!tr ;
  in
    
  let rec foo e1 = 
    if e1 < List.length (list_states ()) 
    then
      begin
	(*print_string " ======   State " ; print_int e1 ; print_string ": " ; 
	List.iter (fun e -> print_int e ; print_string " " ) (get_ith_state e1) ; 
	print_newline () ; 
	if not (List.exists (fun s -> !final=s) (get_ith_state e1))
	then (print_int e1 ; print_string " is not final !\n") ; (* Hashtbl.add is_final s () ; *) *)

	for e2 = 0 to e1 do
	  for l = 0 to nb_labels-1 do
	    dostate e1 e2 l ;
	    if e1 <> e2 then dostate e2 e1 l 
	  done
	done ;
	foo (e1+1)
      end
  in
  foo 0 ;
  let labels = Array.of_list ((List.rev(List.map trad (list_labels())))) in
  print_int (Array.length labels) ; 
  Array.iter (fun s -> print_string (" "^s)) labels;
  print_newline() ;
  print_int (List.length (list_states())) ;
  print_string " 0 0 X " ;
  print_int (List.length (!tr)) ;
  print_newline() ;
  
  
