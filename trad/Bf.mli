type t = And of t * t | Or of t * t | Litt of int * bool | Top | Bot
type af = Af_Prog of int * t | Af_Atom of string
val neg : t -> t
val xor : bool -> t -> t
val replace : int -> t -> t -> t
val simplify : t -> t
val print : t -> unit
val af_print : af -> unit
val get_min_var : t -> int
