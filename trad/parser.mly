/* Analyseur syntaxique pour le mu-calcul */

%{
  open Mu
%}

%token <int> PROG
%token <string> IDENT
%token LET EQUAL IN MU POINT
%token EOF
%token LEFTPAR RIGHTPAR LEFTPROG RIGHTPROG
%token AND OR NOT TRUE
%token COMMA VAR

/* Définitions des priorités et associativités des tokens */

%left OR
%left AND

%start formfile

%type <Mu.formula> formfile
%%
formfile:
| f = form EOF
    { f }
;
form:
| e1 = form AND e2 = form
    { And(e1, e2)}
| e1 = form OR e2 = form
    { Or(e1, e2)}
| LET l = separated_list(COMMA,dec) IN VAR f = IDENT
    { Mu(l,f)}
| MU l = IDENT POINT f=form 
			 { Mu([(l,f)],l)}
| MU VAR l = IDENT POINT f=form 
    { Mu([(l,f)],l)}
| f = subform
    { f }
;
subform:
| LEFTPROG i=PROG RIGHTPROG f=subform
    { Prog(i,f)}
| NOT e = subform
    { Neg(e)}
| VAR s = IDENT
    { Var(s) }
| s = IDENT
    { Atom(s) }
| LEFTPAR f = form RIGHTPAR
    { f }
| TRUE
    { True }
dec:
| VAR s=IDENT EQUAL f = form
   { (s,f) }
;
