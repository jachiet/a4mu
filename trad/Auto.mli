val set_final : Sol.i -> unit
val add_transition : string list -> Sol.i -> Sol.i -> Sol.i -> int -> unit
val get_automaton : (string list -> string) -> unit
val show_automaton : (string list -> string) -> unit
