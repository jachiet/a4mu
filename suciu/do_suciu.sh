#!/bin/bash

./trad.sh e1 > e1.mu
(echo '~' ; cat e1.mu) > ne1.mu 
./trad.sh e2 > e2.mu
time (../trad/solver e2.mu compress ; ../trad/solver ne1.mu compress )| ../intersect/inter
(cat ne1.mu ; echo '&' ; cat e2.mu) > ne1e2
echo ""
echo "--------------------------------------------------"
echo -n "The full intersection ..."
time (../trad/solver e2.mu compress ; ../trad/solver ne1.mu compress )| ../interAuto/inter > /dev/null
echo ""
echo "--------------------------------------------------"
echo -n "The other method ..."

/usr/lib/jvm/java-8-openjdk/bin/java -jar solver.jar ne1e2 | sed -n 's/Computing Fixpoint\.*\[\(.*\)\]/\1/p'
