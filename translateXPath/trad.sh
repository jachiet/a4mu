#!/bin/bash

/usr/lib/jvm/java-8-openjdk/bin/java -jar ../solver.jar $1 -showFormula  |
    grep -A 1000 "Satisfiability Tested Formula:" |
    grep -v "Satisfiability Tested Formula:" |
    grep -B 1000 "Computing Relevant Closure" |
    grep -v "Computing Relevant Closure" |
    sed "s/.context/T/g"
