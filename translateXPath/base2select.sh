#!/bin

rm -rf select/
rm -rf mu/
mkdir select/
mkdir mu/

for i in base/* ; do
    echo "Doing $i" ;
    (echo -n 'select("' ; cat $i ; echo '")' ) > select/${i##base}
    ./trad.sh select/${i##base} >  mu/${i##base}
done
