#!/bin/bash

for i in *.pl ; do
    for k in $(seq 1 10); do
	perl $i xpath ${k} | sed 's/doc()//' > ${i::3}.${k} ;
    done ;
done
