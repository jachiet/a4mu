#!/usr/bin/perl -w

# Query generator for 
# B13(i) = X + Y(i) for i = 0,1,...
# X = //keyword
# Y(i) = (/ancestor::parlist/descendant::keyword)^i

# query components
my $X = "//keyword";
my $Y = "/ancestor::parlist/descendant::keyword";
my $Z = "";

# help message
my $help = "Usage: perl B13.pl type k
type (either xpath or xslt) is the query type
k (>=0) is the query complexity\n";

if (($#ARGV + 1) ne 2) {print $help; exit;}

# type of query
my $type = $ARGV[0];
if (($type ne "xpath") and ($type ne "xslt")) {print $help; exit;}

# query complexity
my $k = $ARGV[1];
if ($k < 0) {print $help; exit;}

my $query = $X;
for ($i = 1; $i <= $k; $i++) {$query = $query.$Y;}
$query = $query.$Z;

if ($type eq "xpath") {
  $query = "doc()".$query;
}

if ($type eq "xslt") {
  my $head = "<?xml version=\"1.0\"?>
  <xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">
    <xsl:template match=\"/\">
      <xsl:copy-of select=\"";
  my $foot = "\"/>
    </xsl:template>
  </xsl:stylesheet>";
  $query = $head.$query.$foot;
}

print $query;