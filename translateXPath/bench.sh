#!/bin/bash

rm -rf res/ ;
mkdir res ;
for i in mu/* ; do
    echo  "${i##mu/}" 
    tim=$((time -p (../trad/solver $i stats compress > res/${i##mu/})) 2>&1 | grep user | sed 's/user//' | tr -d "\n" | tr -d " ")
    echo $tim >> res/${i##mu/}
done ;
