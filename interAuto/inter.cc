#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <set>
#include <deque>
#include <queue>


using namespace std ;

bool extendA=true, extendB=true;

void copy( vector<int> & a, vector<int> & b)
{
  a.insert(a.end(),b.begin(),b.end());
  a.resize(distance(a.begin(),unique(a.begin(),a.end())));
  b.clear();
}


struct automaton
{
  int start1 ;
  int start2 ;
  int end ;
  int nb_states,nb_labels ;
  vector<vector<vector<int> > > trans ;
  vector<string> labels ;
  map<string,int> assoc ;
  int other ;
  vector<int> rename ;
  
  automaton ()
  {
    other = -1 ;
    char buf[200] ;
    int nb;
    scanf("%d",&nb_labels);
    for(int l = 0 ; l < nb_labels ; l++)
      {
        scanf("%s",buf);
        assoc[string(buf)] = labels.size() ;
        if(string(buf) == string("_other"))
          other = labels.size();
        labels.push_back(string(buf));
      }  
    if(other<0)
      {
	other = labels.size() ;
	labels.push_back("_other");
	nb_labels++;
      }
    scanf("%d %d %d %d %d\n",&nb_states,&start1,&start2,&end,&nb);
    trans.resize(nb_labels);
    for(int i = 0 ; i < nb_labels ; i++)
      trans[i].resize(nb_states*nb_states) ;
    while(nb--)
      {
        int e1, e2, e3;
        scanf("%s %d %d %d",buf,&e1,&e2,&e3);
        trans[assoc[buf]][e1*nb_states+e2].push_back(e3);
      }
  }
  
  void to_rename(string s,int from)
  {
    int dest = other ;
    if(assoc.find(s) != assoc.end())
      dest = assoc[s] ;
    rename[from] = dest ;
  }

  vector<int> & get_trans(const int e1, const int e2, const int l)
  {
    return trans[rename[l]][(e1*nb_states+e2)];
  }

  void merge(int l)
  {
    //printf("Merging %s \n",labels[l].c_str());
    for(int e1 = 0 ; e1 < nb_states ; e1++ )
      for(int e2 = 0 ; e2 < nb_states ; e2++ )
	{
	  copy(trans[other][e1*nb_states+e2],trans[l][e1*nb_states+e2]);
	}
    labels[l] = "_other";
  }

  void harmonize_labels( automaton & b, bool extend )
  {
    for(size_t i = 0 ; i < b.labels.size() ; i++ )
      {
	if(assoc.find(b.labels[i]) == assoc.end())
	  {
	    if(extend)
	      {
		assoc[b.labels[i]] = labels.size();
		labels.push_back(b.labels[i]);
		trans.push_back(trans[other]);
	      }
	    else
	      b.merge(i);
	  }
      }
  }
  
} ;

vector<string> all_labels ;
automaton a, b;

void compute_labels()
{
  a.harmonize_labels(b,extendA);
  b.harmonize_labels(a,extendB);
  all_labels = a.labels ;
  all_labels.insert(all_labels.end(),b.labels.begin(),b.labels.end());
  sort(all_labels.begin(),all_labels.end());
  all_labels.resize(distance(all_labels.begin(),unique(all_labels.begin(),all_labels.end())));

  a.rename.resize(all_labels.size());
  b.rename.resize(all_labels.size());

  printf("%lu",all_labels.size());
  
  for(size_t i = 0 ; i < all_labels.size() ; i++ )
    {
      printf(" %s",all_labels[i].c_str());
      a.to_rename(all_labels[i],i);
      b.to_rename(all_labels[i],i);
    }
  printf("\n");
}

typedef tuple<size_t,size_t,size_t> rule;
vector<pair<int,int>> list_states ;
map<pair<int,int>,int> states;
vector<set<rule>> rules;

size_t get_state(size_t a, size_t b)
{
  pair<int,int> s = make_pair(a,b);
  if(states.find(s) == states.end())
    {
      states[s] = list_states.size() ;
      list_states.push_back(s);
      rules.push_back(set<rule>());
    }
  return states[s];
}

void get_rule(size_t x, size_t l, size_t y)
{
  for(size_t r1 : a.get_trans(list_states[x].first,list_states[y].first,l))
    for(size_t r2 : b.get_trans(list_states[x].second,list_states[y].second,l))
      rules[get_state(r1,r2)].insert(rule(x,l,y));
}

vector<bool> useful ;
int nb ;

void dfs(size_t n)
{
  if(useful[n])
    return ;
  useful[n] = true;
  nb++;
  for(rule r:rules[n])
    dfs(get<0>(r)),dfs(get<2>(r));
}

void inter()
{
  size_t s1=get_state(a.start1,b.start1);
  size_t s2=get_state(a.start2,b.start2);
  size_t end=get_state(a.end,b.end);
  for(size_t i = 0 ; i < list_states.size() ; i++ )
    for(size_t j = 0 ; j <= i ; j++ )
      for(size_t l = 0 ; l < all_labels.size() ; l++ )
	{
	  get_rule(i,l,j);
	  get_rule(j,l,i);
	}
  useful.resize(list_states.size());
  dfs(end);
  size_t nb_rules = 0;
  for(size_t res = 0 ; res < rules.size(); res++)
    if(useful[res])
      nb_rules+=rules[res].size();
  printf("%lu %lu %lu %lu %lu\n",list_states.size(),s1,s2,end,nb_rules);
  
  for(size_t res = 0 ; res < rules.size(); res++)
    if(useful[res])
      for(rule r:rules[res])
	printf("%s %lu %lu %lu\n",all_labels[get<1>(r)].c_str(),get<0>(r),get<2>(r),res);  
}

int main(int argc, char ** argv)
{
  compute_labels();
  inter();
  //fprintf(stderr,"Useful %d / %lu\n",nb,useful.size());
  return 0;
}
