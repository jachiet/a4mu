rm raw.bench
for i in tr*orm ; do
    T1=$(date +"%s%N") ;
    echo "Benchmarking $i" ;
    ((cat ../xhtml_strict.auto ; ../trad/solver $i compress) | ../intersect/inter) ;
    T2=$(date +"%s%N") ;
    echo $i $(expr $T2 - $T1) | tee -a raw.bench | awk ' {print $1 " " ($2/1000000) "ms"}' ;

done

cat raw.bench  | grep form | sed 's/tr\(.*\).form/\1/' | awk '{ print "(" $1 "," ($2 / 1000000 ) ")" ; }'
