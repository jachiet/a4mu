let rec show n =
  if n <= 1
  then (print_string "<1>(let $X1 = <1>$X1 | <2>$X1 | tr in $X1)")
  else (print_string "<1>(let $X" ; print_int n ; print_string " = (<1>$X" ; print_int n ; print_string " | <2>$X" ; print_int n ; print_string " | ( tr & (" ; show (n-1) ; print_string "))) in $X" ;print_int n ; print_string ")")
    
let n = read_int()

let _ = show n ; print_newline ()
