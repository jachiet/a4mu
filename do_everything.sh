#!/bin/bash

echo "Compiling the solver" ;
cd trad ;
make ;
echo "Compiling the intersection program" ;
cd ../intersect ;
make ;
cd ../interAuto ;
make ;

echo "=========================================="
echo "Running benchmark from tree calculus paper"
echo "=========================================="
cd ../extreecalc ;
bash transl.sh


echo "=========================================="
echo "     Running benchmark XPathMark "
echo "=========================================="
cd ../translateXPath ;
bash bench.sh


echo "=========================================="
echo "     Running benchmark U_n = (//tr/*)^n"
echo "=========================================="
cd ../gen_form ;
bash do_bench.sh

